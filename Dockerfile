FROM guilhermeadc/sei3_httpd-2.4
MAINTAINER Alexandre and Elton's car <alexandre.molina@embrapa.br>

############################# INÍCIO DA INSTALAÇÃO #############################
ENV TERM xterm

# Arquivos de código fonte do SEI
COPY SEI-Fontes-v3.1.0 /opt

# Cria pastas temps em /opt
RUN mkdir -p /opt/sei/temp /opt/sip/temp

##################### FIM DA INSTALAÇÃO #####################

EXPOSE 80
CMD ["/entrypoint.sh"]
